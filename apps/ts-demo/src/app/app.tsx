import { Route } from 'react-router-dom';
import {
  ReactFc,
  Enum,
  Interface,
  List,
  StaticType,
  ReactClass,
} from '@react-ts/ui';
import { Layout, Menu } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from '@ant-design/icons';
import React from 'react';

const { Header, Sider, Content } = Layout;

export default class App extends React.Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <Layout className="main-layout">
        {/* <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          className="slide-menu"
        >
          <div className="logo"></div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            <Menu.Item key="1" icon={<UserOutlined />}>
              nav 1
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              nav 2
            </Menu.Item>
            <Menu.Item key="3" icon={<UploadOutlined />}>
              nav 3
            </Menu.Item>
          </Menu>
        </Sider> */}
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(
              this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: 'trigger',
                onClick: this.toggle,
              }
            )}
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: '900px',
            }}
          >
            <>
              <Route path="/" exact component={List} />
              <Route path="/static-type" exact component={StaticType} />
              <Route path="/interface" exact component={Interface} />
              <Route path="/enum" exact component={Enum} />
              <Route path="/react-fc" exact component={ReactFc} />
              <Route path="/react-class" exact component={ReactClass} />
            </>
          </Content>
        </Layout>
      </Layout>
    );
  }
}
