import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, Store, Action } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { reducers } from '@react-ts/ui';

import App from './app/app';

const store: Store<unknown, Action<any>> = createStore(
  reducers.todoApp,
  composeWithDevTools()
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);
