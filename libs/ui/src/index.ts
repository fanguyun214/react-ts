import todoApp from './lib/store/reducers';

export * from './lib/containers/react-class/react-class';
export * from './lib/containers/react-fc/react-fc';
export * from './lib/containers/enum/enum';
export * from './lib/containers/interface/interface';
export * from './lib/containers/static-type/static-type';
export * from './lib/containers/list/list';

export const reducers = {
  todoApp,
};
