import { GlobalTodo } from '../../model/state';

interface IType {
  type: string;
}

interface IAddTodo extends IType {
  id: number;
  text: string;
}

interface ISetVisibilityFilter extends IType {
  filter: string;
}

interface IToggleTodo extends IType {
  id: number;
}

let nextTodoId = 0;

export const addTodo = (text: string): IAddTodo => {
  return {
    type: GlobalTodo.TodoActionType.ADD_TODO,
    id: nextTodoId++,
    text,
  };
};

export const setVisibilityFilter = (filter: string): ISetVisibilityFilter => {
  return {
    type: GlobalTodo.FilterActionType.SET_VISIBILITY_FILTER,
    filter,
  };
};

export const toggleTodo = (id: number): IToggleTodo => {
  return {
    type: GlobalTodo.TodoActionType.TOGGLE_TODO,
    id,
  };
};
