import { AnyAction, CombinedState, combineReducers, Reducer } from 'redux';
import { GlobalTodo } from '../../model/state';
import todoList from './todoList';
import visibilityFilter from './visibilityFilter';

const todoApp: Reducer<
  CombinedState<{
    todoList: GlobalTodo.Todo[];
    visibilityFilter: string;
  }>,
  GlobalTodo.ITodoAction & GlobalTodo.IFilterAction
> = combineReducers({
  todoList,
  visibilityFilter,
});

export default todoApp;
