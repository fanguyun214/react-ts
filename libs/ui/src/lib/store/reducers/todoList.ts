import { GlobalTodo } from '../../model/state';

const todoList = (
  state: [],
  action: GlobalTodo.ITodoAction
): GlobalTodo.Todo[] => {
  switch (action.type) {
    case GlobalTodo.TodoActionType.ADD_TODO:
      return [
        ...state,
        {
          id: action.id,
          text: action.text,
          completed: false,
        },
      ];
    case GlobalTodo.TodoActionType.TOGGLE_TODO:
      return state.map((todo: GlobalTodo.Todo) =>
        todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );
    default:
      return state || [];
  }
};

export default todoList;
