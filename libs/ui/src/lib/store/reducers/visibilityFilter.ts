import { GlobalTodo } from '../../model/state';

const visibilityFilter = (
  state = GlobalTodo.FilterType.SHOW_ALL,
  action: GlobalTodo.IFilterAction
): GlobalTodo.FilterType | string => {
  switch (action.type) {
    case GlobalTodo.FilterActionType.SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state;
  }
};

export default visibilityFilter;
