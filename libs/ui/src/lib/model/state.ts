import { Action } from 'redux';

export namespace GlobalTodo {
  export interface Todo {
    // 编号
    id: number;
    // 状态
    completed: boolean;
    // 文本值
    text: string;
  }

  // 筛选类型
  export enum FilterType {
    SHOW_COMPLETED = 'SHOW_COMPLETED',
    SHOW_ACTIVE = 'SHOW_ACTIVE',
    SHOW_ALL = 'SHOW_ALL',
  }

  export enum TodoActionType {
    ADD_TODO = 'ADD_TODO',
    TOGGLE_TODO = 'TOGGLE_TODO',
  }

  export enum FilterActionType {
    SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER',
  }

  // todoAction
  export interface ITodoAction extends Action {
    id: number;
    text: string;
    type: TodoActionType;
  }

  // filterAction
  export interface IFilterAction extends Action {
    filter: FilterType;
    type: FilterActionType;
  }

  // todoListState
  export interface IState {
    // 列表
    todoList: Array<Todo> | [];
    // 筛选状态
    visibilityFilter: FilterType;
  }
}
