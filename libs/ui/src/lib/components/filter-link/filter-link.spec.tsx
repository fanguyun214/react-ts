import { render } from '@testing-library/react';

import FilterLink from './filter-link';

describe('FilterLink', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FilterLink />);
    expect(baseElement).toBeTruthy();
  });
});
