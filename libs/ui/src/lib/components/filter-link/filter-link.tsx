import { connect } from 'react-redux';
import { setVisibilityFilter } from '../../store/actions';
import { GlobalTodo } from '../../model/state';
import Link from '../link/link';
import { Dispatch } from 'redux';

interface IOwnProps {
  filter: string;
}

const mapStateToProps = (state: GlobalTodo.IState, ownProps: IOwnProps) => {
  return {
    active: ownProps.filter === state.visibilityFilter,
  };
};

const mapDispatchToProps = (dispatch: Dispatch, ownProps: IOwnProps) => {
  return {
    onClick: () => {
      dispatch(setVisibilityFilter(ownProps.filter));
    },
  };
};

const FilterLink = connect(mapStateToProps, mapDispatchToProps)(Link);

export default FilterLink;
