import React from 'react';

import './link.module.less';

export interface LinkProps {
  active: boolean;
  children: React.ReactNode;
  onClick: () => void;
}

export function Link({ active, children, onClick }: LinkProps): JSX.Element {
  if (active) {
    return <span>{children}</span>;
  }
  return (
    <a
      href=""
      onClick={(e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        e.preventDefault();
        onClick();
      }}
    >
      {children}
    </a>
  );
}

export default Link;
