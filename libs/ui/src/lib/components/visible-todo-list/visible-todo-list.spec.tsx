import { render } from '@testing-library/react';

import VisibleTodoList from './visible-todo-list';

describe('VisibleTodoList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<VisibleTodoList />);
    expect(baseElement).toBeTruthy();
  });
});
