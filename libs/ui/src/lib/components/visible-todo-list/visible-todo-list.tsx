import { connect } from 'react-redux';
import { toggleTodo } from '../../store/actions';
import TodoList from '../todo-list/todo-list';
import { GlobalTodo } from '../../model/state';
import { Dispatch } from 'redux';

const getVisibleTodoList = (
  todoList: GlobalTodo.Todo[],
  filter: GlobalTodo.FilterType
) => {
  switch (filter) {
    case GlobalTodo.FilterType.SHOW_COMPLETED:
      return todoList.filter((t) => t.completed);
    case GlobalTodo.FilterType.SHOW_ACTIVE:
      return todoList.filter((t) => !t.completed);
    case GlobalTodo.FilterType.SHOW_ALL:
    default:
      return todoList;
  }
};

const mapStateToProps = (state: GlobalTodo.IState) => {
  return {
    todoList: getVisibleTodoList(state.todoList, state.visibilityFilter),
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onTodoClick: (id: number) => {
      dispatch(toggleTodo(id));
    },
  };
};

const VisibleTodoList = connect(mapStateToProps, mapDispatchToProps)(TodoList);

export default VisibleTodoList;
