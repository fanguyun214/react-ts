import React from 'react';
import { List, Typography, Divider } from 'antd';
import Todo from '../todo/todo';
import './todo-list.module.less';
import { GlobalTodo } from '../../model/state';

/* eslint-disable-next-line */
export interface TodoListProps {
  todoList: Array<GlobalTodo.Todo>;
  onTodoClick: (arg: number) => void;
}

export function TodoList({
  todoList,
  onTodoClick,
}: TodoListProps): JSX.Element {
  if (!todoList || !todoList.length) {
    return null;
  }
  return (
    <List
      size="small"
      header={null}
      footer={null}
      bordered
      dataSource={todoList}
      style={{ margin: '5px 0' }}
      renderItem={(todo: GlobalTodo.Todo) => (
        <Todo key={todo.id} {...todo} onClick={() => onTodoClick(todo.id)} />
      )}
    />
  );
}

export default TodoList;
