import React from 'react';

import './todo.module.less';

export interface TodoProps {
  onClick: () => void;
  completed: boolean;
  text: string;
}

export function Todo({ onClick, completed, text }: TodoProps): JSX.Element {
  return (
    <li
      onClick={onClick}
      style={{
        textDecoration: completed ? 'line-through' : 'none',
				lineHeight: '35px',
				border: '1px solid #efefef',
				padding: ' 0 5px'
      }}
    >
      {text}
    </li>
  );
}

export default Todo;
