import React, { useState } from 'react';
import { connect } from 'react-redux';
import { addTodo } from '../../store/actions';
import { Input, Button, message } from 'antd';
import { Dispatch } from 'redux';

interface IAddTodoProps {
  dispatch?: Dispatch;
}

let AddTodo = ({ dispatch }: IAddTodoProps): JSX.Element => {
  const [initValue, setValue] = useState<string>('');

  const handleAdd = (e: React.MouseEvent<HTMLButtonElement>): void => {
    e.preventDefault();
    if (!initValue.trim()) {
      message.info('请输入有效值!');
      return;
    }
    dispatch(addTodo(initValue));
    setValue('');
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    setValue(e.target.value);
  };

  return (
    <div>
      <Input value={initValue} style={{ width: 220 }} onChange={handleChange} />
      <Button type="primary" onClick={handleAdd}>
        Add Todo
      </Button>
    </div>
  );
};
AddTodo = connect()(AddTodo);

export default AddTodo;
