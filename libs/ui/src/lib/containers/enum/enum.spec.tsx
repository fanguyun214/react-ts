import { render } from '@testing-library/react';

import Enum from './enum';

describe('Enum', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Enum />);
    expect(baseElement).toBeTruthy();
  });
});
