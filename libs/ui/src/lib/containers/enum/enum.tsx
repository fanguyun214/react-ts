import React from 'react';

import './enum.module.less';

/* eslint-disable-next-line */
export interface EnumProps {}

// 枚举是一种数据类型
enum Color {
  Red,
  Blue,
  Black,
}

// 类型也只能是枚举 Color 类型
const color: Color = Color.Blue;
console.log('color', color); // ==> 打印出下标 1

// 枚举一般首字母大写
enum Status {
  // OFFLINE = 1,
  /* 离线状态 */
  OFFLINE,
  /* 在线状态 */
  ONLINE,
  /* 其他 */
  OTHERS,
}

// 对比常用的 JS 代码
// const Status = {
//   OFFLINE: 0,
//   ONLINE: 1,
//   OTHERS: 2,
// };

function getStatus(status: number): string {
  if (status === Status.ONLINE) {
    return 'online';
  } else if (status === Status.OFFLINE) {
    return 'offline';
  } else if (status === Status.OTHERS) {
    return 'others';
  }
  return 'error';
}

const result = getStatus(Status.OFFLINE);
console.log(result);

// 下面代码直接打印出 enum 的下标值
console.log(Status.OFFLINE);
console.log(Status.ONLINE);
console.log(Status.OTHERS);

// 打印下标对应的属性
console.log(Status[0]);
console.log(Status[1]);
console.log(Status[2]);

export function Enum(props: EnumProps) {
  return (
    <div>
      <h1>Welcome to enum!</h1>
    </div>
  );
}

export default Enum;
