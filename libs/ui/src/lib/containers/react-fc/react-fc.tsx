import * as React from 'react';
import { useCallback } from 'react';
import { useState } from 'react';

interface Props {
  label: string;
  count: number;
}

export const ReactFc: React.FC<Props> = (props) => {
  const { label = 'count', count = 0 } = props;
  const [newCount, setCount] = useState(count);

  const handleCount = useCallback((): void => {
    setCount(newCount + 1);
  }, [newCount]);

  return (
    <div>
      <span>
        {label}: {newCount}
      </span>
      <br />
      <button type="button" onClick={handleCount}>
        {`ChangeCount`}
      </button>
    </div>
  );
};
