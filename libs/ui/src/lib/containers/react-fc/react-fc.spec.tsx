import { render } from '@testing-library/react';

import ReactFc from './react-fc';

describe('ReactFc', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ReactFc />);
    expect(baseElement).toBeTruthy();
  });
});
