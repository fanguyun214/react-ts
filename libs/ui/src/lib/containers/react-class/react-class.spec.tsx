import { render } from '@testing-library/react';

import ReactClass from './react-class';

describe('ReactClass', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ReactClass />);
    expect(baseElement).toBeTruthy();
  });
});
