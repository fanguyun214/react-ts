import React from 'react';

interface Props {
  label: string;
}
interface State {
  count: number;
}

export class ReactClass extends React.Component<Props, State> {
  readonly state: State = {
    count: 0,
  };
  handleCount = (): void => {
    this.setState({ count: this.state.count + 1 });
  };
  render() {
    const { handleCount } = this;
    const { label = 'count' } = this.props;
    const { count } = this.state;
    return (
      <div>
        <span>
          {label}: {count}
        </span>
        <br />
        <button type="button" onClick={handleCount}>
          {`ChangeCount`}
        </button>
      </div>
    );
  }
}
