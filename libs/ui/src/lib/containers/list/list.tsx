import React from 'react';
import { Footer } from '../../components/footer/footer';
import AddTodo from '../../components/add-todo/add-todo';
import VisibleTodoList from '../../components/visible-todo-list/visible-todo-list';
import './list.module.less';

export function List(): JSX.Element {
  return (
    <div>
      <AddTodo />
      <VisibleTodoList />
      <Footer />
    </div>
  );
}

export default List;
