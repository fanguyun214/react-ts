import React from 'react';

import './interface.module.less';

/* eslint-disable-next-line */
export interface InterfaceProps {}

// 一个简单实例示例说明 interface 是一个类对象
// 还能定义以后可能用到的属性，在属性后面加上'?'，类似函数可选参数，如示例
// 可能用到的属性，在属性后面加上'?'，
interface Person {
  name: string;
  age: number;
  age1?: number; // age1 是接口可能用到的属性。
  readonly ID: number;
}

const person: Person = {
  name: 'LinYY',
  age: 18,
  // age1: 0,
  ID: 101,
};
// 只读属性 能对属性定义进行只读操作 readonly，在对应属性面前加上 readonly 就能限定只读操作。只读属性被初始化后的值不能在被修改。
// 只读操作 readonly 不传入 age1 也可以通过校验
const getName = (person: Person) => {
  // Person 是上面的 interface 接口
  console.log(person.age);
  // console.log(person.age1)  ==> 0
  console.log(person.ID);
  // person.ID = 200     // 报错 ID只能读取，不能修改==> error TS2540: Cannot assign to 'ID' because it is a read-only property
};

getName(person);
// interface 中还能定义方法，跟着的类型表示返回值类型。
// interface 中定义方法
interface Search {
  (a: number, b: number): boolean;
}

const search: Search = function (a: number, b: number): boolean {
  return a >= b;
};
search(2, 3);

// 或
interface Action {
  name: string;
  age: number;
  say(): string;
}
const action = {
  name: 'LinYY',
  age: 18,
  say() {
    return 'hello TS';
  },
};
const applySay = (action: Action) => {
  console.log(action.say());
};
applySay(action);

// 下面定义了一个 say 类型的接口 接受一个 string 的参数，返回 string 类型的字符串
interface Say {
  (word: string): string;
}

const foo: Say = (word: string) => {
  return word;
};

foo('hello TS'); //  ==> 'hello TS'
// interface 间还能相互嵌套
/**interface 间还能相互嵌套 */
interface A {
  name: string;
  age: number;
}

interface B {
  /** person is interface a */
  person: A;
}
// 也可以被类 class 通过 implements 使用，实现（implements）是面向对象中的一个重要概念，简单点理解就是实现接口 interface 中属性和方法，详情参考官网 implements
// implements 和 extend 不同 extend 是继承父类，implement 是实现接口 interface 而且可以使用多个接口，用逗号隔开。
// class A extends B implements C,D,E

interface Person {
  name: string;
  age: number;
  age1?: number; // age1 是接口可能用到的属性。
  readonly ID: number;
}

class test implements Person {
  name = 'LinYY';
  age = 18;
  ID = 301;
}
// interface 还可以被其他接口继承 extends
// 被其他接口继承 extends
interface Music {
  click: boolean;
}

interface Sentence {
  color: string;
}

interface Classic extends Music, Sentence {
  time: number;
}

const classic = {} as Classic;
classic.click = false;
classic.color = 'white';
classic.time = 220;

// 或
interface PersonB {
  name: string;
  age: number;
  age1?: number; // age1 是接口可能用到的属性。
  readonly ID: number;
}

interface Teach extends PersonB {
  action(): string;
}

const teach: Teach = {
  name: 'LinYY',
  age: 28,
  ID: 501,
  action() {
    return '222';
  },
};

// 注意点
// interface 接口中不强制规定后来加上的属性，只要传入参数满足 interface 接口已有的属性，也能通过 如示例二，age，sex 属性不在接口 Person 中也能通过校验
interface Obj {
  name: string;
}
function printName(obj: Obj) {
  console.log('Orville Ideas and Interests');
}

const myObj = { name: 'LinYY', age: 18 };
// 不在接口内，也可以通过校验
printName(myObj);
// 但是以字面量形式传入不在接口中的属性，TS 会强校验导致报错。
// printName({ name: 'LinYY', age: 18 });

// interface 实际上在编译成 JS 后并没有相应的代码，其实 interface 就是 TS 来约束代码代码规范的。
// 怎么有效解决以后开发的过程中可能会加入的属性呢？
// 可以直接在 interface 接口中加入以后可能会用到的属性如，string 类型 [propName: string]: any，如示例三
// 示例三 [propName: string]: any
interface User {
  name: string;
  age: number;
  [propName: string]: unknown; // 后期可能用到的属性
}

const getSex = (user: User) => {
  console.log(user.age);
};

const user = {
  name: 'LinYY',
  age: 18,
  sex: 'male', // 不在 User 接口内，但也能通过
};

getSex(user);

// 以字面量的形式传入也可以。
getSex({
  name: 'LinYY',
  age: 18,
  sex: 'male',
});

// interface 和 type alias 关系
// 两者用法上面没有太大的区别，同样都可以扩展，只是语法不同，type 使用交叉类型 &。而且两者相互之间可以继承。
// 但是 interface的应用场景更加的广，能够使用interface就不用type
// interface 和 type alias
/** ifc is interface */
interface Ifc {
  name: string;
  age: number;
}

/** T is type alias */
type T = {
  name: string;
  age: number;
};

/* interface 和 type alias 扩展示例 */
interface IfcName {
  name: string;
}
interface IfcAge extends IfcName {
  age: number;
}

type TName = {
  name: string;
};

/** TAge 继承了 TName 的 name 属性*/
type TAge = TName & {
  age: number;
};
const tAge: TAge = {
  name: 'LinYY',
  age: 18,
};

/* interface 和 type alias 相互继承示例 */

/** interface  extends type alias */
interface IfcAge extends TName {
  age: number;
}

/** type & interface alias */
type TypeName = IfcAge & {
  name: string;
};

/* type alias 定义常量 */
type Const = 2;

/* type alias 定义元组 */
type Tuple = [number, string];

export function Interface(props: InterfaceProps) {
  return (
    <div>
      <h1>Welcome to interface!</h1>
    </div>
  );
}

export default Interface;
