import { render } from '@testing-library/react';

import StaticType from './static-type';

describe('StaticType', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<StaticType />);
    expect(baseElement).toBeTruthy();
  });
});
