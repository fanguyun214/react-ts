import React from 'react';

import './static-type.module.less';

// 基础类型 number string null undefined symbol boolean any void never。。。 * number 类型
const num: number = 123;
// string 类型
const Name: string = 'LinYY';
// boolean 类型
const boolean: Boolean = true;
// null 类型 。null 类型不可以赋值给 undefined 类型和 联合类型（后面介绍）
const n: null = null;
// undefined 类型
const u: undefined = undefined;
// undefined 类型。可以作用到可选类型，因为可选的类型默认会有一个undefined 类型
interface E {
  b: number;
  c?: number;
}

let e: E = { b: 12, c: 12 };
e = { b: 23, c: undefined };

// any 类型。已经定义变量的类型不能再修改，否则报错。 注意 any 类型，any 类型定义后可以修改为其他的类型
// any 类型可以修改成其他任何类型，TS 不对 any 类型作类型检测
let not: any;
not = 2;
not = '2';
not = true;

// 处理不确定的数组类型 any 比较合适。
const listArr: any[] = ['1', 2, true];

// void 类型 和 any 类型相反，表示没有任何类型 void 类型 通常作用在函数中代表没有返回值
// void 空类型，一般用于函数，
function noReturn(): void {
  console.log('no value return');
}

// function fn(): void {
//   // Error
//   return 3;
// }

function fn5(): void {}
// const un: undefined = fn5(); // Error 不能将类型“void”分配给类型“undefined”

// 对象类型 object type。object {}，array [], class {}， function
const person: {
  name: string;
  age: number;
} = {
  name: 'LinYY',
  age: 12,
};

// 数组类型 也是对象类型，下面声明number型数组只能写入数字来初始化，写入字符串将会报错。
const list: number[] = [12, 23, 34];

//等同于
const listA: Array<number> = [1, 2, 3];

// function 函数类型, 下面的函数类型要求返回值是 number 数字类型，写成其他类型如 string 会报错。
const getNumber: () => number = () => {
  // return 'LinYY'   报错
  return 123;
};

// 要求返回值是string 字符类型
const getString: () => string = () => {
  return 'LinYY';
  // return 123
};

// interface 自定义类型，也就是接口
interface Point {
  x: number;
  y: number;
}

const point: Point = {
  x: 2,
  y: 4,
};

// 多类型。变量的类型可以有多个，比如可以是 number 或 string类型
// 变量的类型可以有多个，比如可以是number或string类型。
let temp: number | string = 23;
temp = '23';

// type alias 类型别名
type User = { name: string; age: number };
const male: User = { name: 'LinYY', age: 18 };
const famale: User = { name: 'nana', age: 18 };

// 注释小技巧tip: 使用 /** */ 可以给类型添加更友好的提示
/**
 * 用户卡片信息
 */
interface Per {
  // 姓名
  name: string;
  // 年龄
  age: 20;
  // 城市
  city: 'ShenZhen';
}

// const p: Per = {
//   name: 'LinYY',
// };

export function StaticType(props: {}) {
  return (
    <div>
      <h1>Welcome to static-type!</h1>
    </div>
  );
}

export default StaticType;
